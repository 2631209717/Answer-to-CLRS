class UnderFlow(Exception):
    pass


class Maxheap:
    """ a max heap
    """
    def __init__(self):
        self._array = None
        self._heapsize = None
        self._currentindex = 0

    def __iter__(self):
        return self

    def __next__(self):
        return self.Extract_max()

    def left(self, i):
        return 2*i

    def right(self, i):
        return 2*i + 1

    def parent(self, i):
        return i//2

    def Max_heapify(self, i):
        l = self.left(i)
        r = self.right(i)
        largest = i
        while l <= self._heapsize:
            if self._array[l] > self._array[largest]:
                largest = l
            if r <= self._heapsize and self._array[r] > self._array[largest]:
                largest = r
            if largest == i:
                break
            else:
                self._array[i], self._array[largest] = \
                    self._array[largest], self._array[i]
                i = largest
                l = self.left(i)
                r = self.right(i)

    def Build_max_heap(self, array):
        self._array = [0]
        self._array.extend(array[:])
        self._heapsize = len(array)
        for i in range(1, self._heapsize//2 + 1)[::-1]:
            self.Max_heapify(i)

    def Max(self):
        return self._array[0]

    def Extract_max(self):
        if self._heapsize < 1:
            raise UnderFlow
        maximum = self._array[1]
        self._array[1] = self._array[self._heapsize]
        self._heapsize -= 1
        self.Max_heapify(1)
        return maximum

    def Increase_key(self, i, key):
        if key < self._array[i]:
            raise Exception("new key is smaller than current key")
        self._array[i] = key
        while i > 1 and self._array[self.parent(i)] < self._array[i]:
            self._array[i], self._array[self.parent(i)] = \
                self._array[self.parent(i)], self._array[i]
            i = self.parent(i)

    def Insert(self, key):
        self._heapsize += 1
        self._array.append(-float('inf'))
        self.Increase_key(self._heapsize, key)


array = [1, 5, 8, 7, 9, 3, 5, 6, 15, 2]
t = Maxheap()
t.Build_max_heap(array)
